﻿using OpenTK;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace GravityModel
{
    public partial class FormVisualSettings : Form
    {
        public FormVisualSettings()
        {
            InitializeComponent();
        }

        private void UpdateData()
        {
            OpenGLRenderer currRender = Form1.GetCurrentRenderer();
            DirectionalLight currLight = currRender.directionalLight;
            ambientTrack.Value = (int)(currLight.AmbientIntensity * 10);
            diffuseTrack.Value = (int)(currLight.DiffuseIntensity * 10);
            xLightNumericUpDown.Value = (decimal)currLight.Direction.X;
            yLightNumericUpDown.Value = (decimal)currLight.Direction.Y;
            zLightNumericUpDown.Value = (decimal)currLight.Direction.Z;
            ColorPanel.BackColor = Color.FromArgb((int)(255 * currLight.Color.X), (int)(255 * currLight.Color.Y), (int)(255 * currLight.Color.Z));

            FOVnumericUpDown.Value = (decimal)(currRender.FOV / Math.PI) * 180;
            viewDistNumericUpDown.Value = (decimal)currRender.viewDistance;

            Axis currAxis = currRender.GetMainAxis();
            axisLenghtNumericUpDown.Value = (decimal)currAxis.length;
            axisDivisionNumericUpDown.Value = (decimal)currAxis.division;
            axisDivisionLenghtNumericUpDown.Value = (decimal)currAxis.divisionLength;
            axisFontSizeNumericUpDown.Value = (decimal)currAxis.font.Size;

            tailLengthNumericUpDown.Value = (decimal)Body.maxTailLength;
            tailNextPointAfterNumericUpDown.Value = (decimal)Body.nextPointAfter;

            Camera currCamera = currRender.GetActiveCamera();
            cameraTranslateSpeedumericUpDown.Value = (decimal)currCamera.translateSpeedModule;
            cameraRotateSpeedNumericUpDown.Value = (decimal)currCamera.rotateSpeedModule;
            cameraZoomSpeedNumericUpDown.Value = (decimal)currCamera.zoomSpeedModule;
        }

        private void FormVisualSettings_Load(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void changeColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                ColorPanel.BackColor = colorDialog1.Color;
            }
        }

        private void lightSaveButton_Click(object sender, EventArgs e)
        {
            DirectionalLight newLight = new DirectionalLight();
            newLight.AmbientIntensity = (float)ambientTrack.Value / 10;
            newLight.DiffuseIntensity = (float)diffuseTrack.Value / 10;
            newLight.Direction = new Vector3((float)xLightNumericUpDown.Value,
                                            (float)yLightNumericUpDown.Value,
                                            (float)zLightNumericUpDown.Value);
            newLight.Color = new Vector3((float)ColorPanel.BackColor.R / 255,
                                         (float)ColorPanel.BackColor.G / 255,
                                         (float)ColorPanel.BackColor.B / 255);

            Form1.GetCurrentRenderer().SetLight(newLight);
        }

        private void saveAxisesButton_Click(object sender, EventArgs e)
        {
            Axis currAxis = Form1.GetCurrentRenderer().GetMainAxis();
            currAxis.Set(
                (float)axisLenghtNumericUpDown.Value,
                (float)axisDivisionNumericUpDown.Value,
                (float)axisDivisionLenghtNumericUpDown.Value,
                new Font(currAxis.font.FontFamily, (float)axisFontSizeNumericUpDown.Value),
                currAxis.brush);
        }

        private void saveTailsButton_Click(object sender, EventArgs e)
        {
            Body.SetTailParams((float)tailLengthNumericUpDown.Value, (float)tailNextPointAfterNumericUpDown.Value);
        }

        private void saveMouseSenseButton_Click(object sender, EventArgs e)
        {
            Form1.GetCurrentRenderer().GetActiveCamera().SetSpeed(
                (float)cameraRotateSpeedNumericUpDown.Value,
                (float)cameraTranslateSpeedumericUpDown.Value,
                (float)cameraZoomSpeedNumericUpDown.Value);
        }

        private void setCameraPositionButton_Click(object sender, EventArgs e)
        {
            Vector3 newPos = new Vector3(
                (float)cameraPositionXnumericUpDown.Value,
                (float)cameraPositionYnumericUpDown.Value,
                (float)cameraPositionZnumericUpDown.Value);
            Camera current = Form1.GetCurrentRenderer().GetActiveCamera();
            Camera newCam = new Camera(newPos, current.target, new Vector3(0, 0, 1));
            Form1.GetCurrentRenderer().SetActiveCamera(newCam);
        }

        private void SetCameraTargetButton_Click(object sender, EventArgs e)
        {
            Vector3 newTarget = new Vector3(
                (float)cameraTargetXnumericUpDown.Value,
                (float)cameraTargetYnumericUpDown.Value,
                (float)cameraTargetZnumericUpDown.Value);
            Camera current = Form1.GetCurrentRenderer().GetActiveCamera();
            Camera newCam = new Camera(current.eye, newTarget, new Vector3(0, 0, 1));
            Form1.GetCurrentRenderer().SetActiveCamera(newCam);
        }

        private void ViewSaveButton_Click(object sender, EventArgs e)
        {
            float currFOV = (float)FOVnumericUpDown.Value / 180 * (float)Math.PI;
            Form1.GetCurrentRenderer().SetupViewport(currFOV, (float)viewDistNumericUpDown.Value);
        }

        private void setDarkButton_Click(object sender, EventArgs e)
        {
            if (SetDarkCheckBox.Checked)
            {
                Form1.GetCurrentRenderer().SetAxisColor(Color.White);
                Form1.GetCurrentRenderer().SetClearColor(Color.Black);
            }
            else
            {
                Form1.GetCurrentRenderer().SetAxisColor(Color.Black);
                Form1.GetCurrentRenderer().SetClearColor(Color.White);
            }
        }
    }
}
