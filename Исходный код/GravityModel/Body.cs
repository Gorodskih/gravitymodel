﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace GravityModel
{
    public class Body
    {
        public double radius;
        public double mass;
        public Vector3d position, speed, force;
        public Color4 color;

        public static double G = (double)6.67E-11;
        public static int Gpower = -11;
        public static double dt = 0.0001f;

        public static int tailVBO = GL.GenBuffer();
        public bool tracking;
        public Queue<float> Tail;
        public double tailLength, distanceAfterLastAddedPoint;
        public static float maxTailLength, nextPointAfter;
        private Vector3d prevPos;

        public object editingTail;


        public Body(bool _tracking, double _radius, double _mass, Vector3d _position, Vector3d _speed, Color4 _color)
        {
            mass = _mass;
            radius = _radius;
            position = _position;
            speed = _speed;
            tracking = _tracking;
            force = new Vector3d(0, 0, 0);
            Tail = new Queue<float>();
            prevPos = new Vector3d(0, 0, 0);
            distanceAfterLastAddedPoint = 0;
            tailLength = 0;
            editingTail = new object();
            color = _color;
        }

        public Body(Body body)
        {
            mass = body.mass;
            radius = body.radius;
            position = new Vector3d(body.position);
            speed = new Vector3d(body.speed);
            tracking = body.tracking;
            force = new Vector3d(body.force);
            Tail = new Queue<float>(body.Tail);
            prevPos = new Vector3d(body.prevPos);
            tailLength = body.tailLength;
            distanceAfterLastAddedPoint = body.distanceAfterLastAddedPoint;
            editingTail = body.editingTail;
            color = body.color;
        }

        public void Move()
        {
            prevPos = new Vector3d(position);

            Vector3d dV = force * dt / mass;
            position += (speed + dV / 2) * dt;
            speed += dV;

            if (tracking)
            {
                distanceAfterLastAddedPoint += (position - prevPos).Length;
                if (distanceAfterLastAddedPoint >= nextPointAfter)
                {
                    Monitor.Enter(editingTail);
                    try
                    {
                        Tail.Enqueue((float)position.X); Tail.Enqueue((float)position.Y); Tail.Enqueue((float)position.Z);
                    }
                    finally
                    {
                        Monitor.Exit(editingTail);
                    }
                    tailLength += nextPointAfter;
                    distanceAfterLastAddedPoint = 0;
                }
                if (tailLength + distanceAfterLastAddedPoint > maxTailLength)
                {
                    Monitor.Enter(editingTail);
                    try
                    {
                        if (Tail.Count >= 3)
                        {
                            Tail.Dequeue();
                            Tail.Dequeue();
                            Tail.Dequeue();
                            tailLength -= nextPointAfter;
                        }
                        else
                        {
                            Tail.Clear();
                            tailLength = 0;
                        }

                    }
                    finally
                    {
                        Monitor.Exit(editingTail);
                    }
                }
            }
        }

        static public void GetForces(List<Body> bodies)
        {
            for (int i = 0; i < bodies.Count - 1; i++)
                for (int j = i + 1; j < bodies.Count; j++)
                {
                    Body first = bodies.ElementAt(i);
                    Body second = bodies.ElementAt(j);
                    Vector3d distance = second.position - first.position;
                    double F = G * first.mass * second.mass / distance.LengthSquared;
                    Vector3d ForceToThis = distance.Normalized() * F;
                    first.force += ForceToThis;
                    second.force -= ForceToThis;
                }
        }

        public void FreeForce()
        {
            force = new Vector3d(0, 0, 0);
        }

        public void CheckAbsorption(List<Body> bodies)
        {
            List<Body> temp = new List<Body>(bodies);
            foreach (Body body in temp)
                if (this != body)
                    if (isTouchingBody(body))
                    {
                        speed = (mass * speed + body.mass * body.speed) / (mass + body.mass);
                        if (body.radius > radius)
                        {
                            color = body.color;
                            Monitor.Enter(editingTail);
                            try
                            {
                                Tail = body.Tail;
                                tailLength = body.tailLength;
                            }
                            finally
                            {
                                Monitor.Exit(editingTail);
                            }
                        }
                        mass += body.mass;
                        radius += body.radius;
                        position = (position + body.position) / 2;
                        bodies.Remove(body);
                    }
        }

        public bool isTouchingBody(Body secondBody)
        {
            double r = (position - secondBody.position).Length;
            if (r < radius + secondBody.radius)
                return true;
            return false;
        }

        public static void SetTailParams(float maxLength, float PointAfter)
        {
            maxTailLength = maxLength;
            nextPointAfter = PointAfter;
        }

        public static List<Body> CopyList(List<Body> oldList)
        {
            List<Body> newList = new List<Body>(oldList.Count);
            oldList.ForEach((item) => { newList.Add(new Body(item)); });
            return newList;
        }

        public static void MoveBodies(List<Body> bodies)
        {
            int i = 0;
            while (i < bodies.Count)
            {
                bodies.ElementAt(i).CheckAbsorption(bodies);
                ++i;
            }
            foreach (Body body in bodies)
                body.FreeForce();
            Body.GetForces(bodies);
            foreach (Body body in bodies)
                body.Move();
        }

    }
}