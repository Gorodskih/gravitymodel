﻿using OpenTK;

namespace GravityModel
{
    public class Pipeline
    {
        private Matrix4 ProjectionMatrix, ScaleMatrix, TranslateMatrix, RotationMatrix;
        public Camera ActiveCamera;

        public Pipeline()
        {
            ProjectionMatrix = Matrix4.Identity;
            ScaleMatrix = Matrix4.Identity;
            TranslateMatrix = Matrix4.Identity;
            RotationMatrix = Matrix4.Identity;
            ActiveCamera = new Camera();
        }

        public Matrix4 ViewMatrix
        {
            get
            {
                return ScaleMatrix * TranslateMatrix * RotationMatrix;
            }
        }

        public Matrix4 WVPMatrix
        {
            get
            {
                return ViewMatrix * ActiveCamera.ViewMatrix * ProjectionMatrix;
            }
        }

        public void SetWorldPosition(float x, float y, float z)
        {
            TranslateMatrix = Matrix4.CreateTranslation(x, y, z);
        }

        public void SetWorldPosition(Vector3 position)
        {
            TranslateMatrix = Matrix4.CreateTranslation(position);
        }

        public void SetScale(float scale)
        {
            ScaleMatrix = Matrix4.CreateScale(scale);
        }

        public void SetRotationMatrix(string rotationOrder, float xAngle, float yAngle, float zAngle)
        {
            Matrix4 X = Matrix4.CreateRotationX(xAngle);
            Matrix4 Y = Matrix4.CreateRotationY(yAngle);
            Matrix4 Z = Matrix4.CreateRotationZ(zAngle);
            RotationMatrix = Matrix4.Identity;
            foreach (char axis in rotationOrder)
                switch (axis)
                {
                    case 'x':
                    case 'X':
                        RotationMatrix *= X;
                        break;
                    case 'y':
                    case 'Y':
                        RotationMatrix *= Y;
                        break;
                    case 'z':
                    case 'Z':
                        RotationMatrix *= Z;
                        break;
                    default: break;
                }
        }

        public void SetRotationMatrix(Matrix4 matrix)
        {
            RotationMatrix = matrix;
        }

        public void SetActiveCamera(Camera camera)
        {
            ActiveCamera = camera;
        }

        public void SetProjectionMatrix(Matrix4 projection)
        {
            ProjectionMatrix = projection;
        }
    }
}
