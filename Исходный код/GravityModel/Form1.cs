﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace GravityModel
{
    public partial class Form1 : Form
    {
        static OpenGLRenderer mainRenderer;
        bool processing = false;
        public static List<Body> loadedBodies, movingBodies;
        Thread calculationThread;
        TimeSpan elapsedTime = new TimeSpan(0);
        public static TimeSpan tickTimespan;
        UInt64 currentIteration;

        public Form1()
        {
            InitializeComponent();
            glControlMain.MouseWheel += new MouseEventHandler(glControlMain_MouseWheel);
            movingBodies = new List<Body>();
            loadedBodies = new List<Body>();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            glControlMain.Height = this.ClientSize.Height - 30;
            glControlMain.Width = this.ClientSize.Width - groupBox1.Width - 30;
            groupBox1.Location = new Point(this.ClientSize.Width - groupBox1.Width - 10, groupBox1.Location.Y);
            groupBox2.Location = new Point(this.ClientSize.Width - groupBox2.Width - 10, groupBox2.Location.Y);
            groupBox3.Location = new Point(this.ClientSize.Width - groupBox3.Width - 10, groupBox3.Location.Y);
            mainRenderer.SetupViewport(mainRenderer.FOV, mainRenderer.viewDistance);
        }


        private void moveBodies()
        {
            while (processing)
            {
                Body.MoveBodies(movingBodies);
                elapsedTime = elapsedTime.Add(tickTimespan);
                currentIteration++;
                if (stopAfterIterationscheckBox.Checked)
                    if (currentIteration >= stopIterationsnumericUpDown.Value)
                        processing = false;         
            }
            BeginInvoke(new MethodInvoker(delegate
            {
                Stop();
            }));
        }

        private void Start()
        {
            if (checkBoxRefresh.Checked)
            {
                movingBodies = Body.CopyList(loadedBodies);
                elapsedTime = new TimeSpan(0);
                currentIteration = 0;
            }
            processing = true;
            buttonStartStop.Text = "Стоп";
            stopAfterIterationscheckBox.Enabled = false;
            calculationThread = new Thread(moveBodies);
            calculationThread.IsBackground = true;
            calculationThread.Start();
        }

        private void Stop()
        {
            processing = false;
            buttonStartStop.Text = "Старт";
            stopAfterIterationscheckBox.Enabled = true;
        }

        private void buttonStartStop_Click(object sender, EventArgs e)
        {
            if (!processing)
                Start();
            else
                Stop();
        }

        private void glControlMain_MouseMove(object sender, MouseEventArgs e)
        {
            mainRenderer.MoveActiveCamera();
        }

        private void renderTimer_Tick(object sender, EventArgs e)
        {
            mainRenderer.Render();
            label2.Text = elapsedTime.ToString();
        }


        private void buttonEditBodies_Click(object sender, EventArgs e)
        {
            FormAddBodies form = new FormAddBodies();
            form.Owner = this;
            form.Show();
        }

        private void visualSettingsButton_Click(object sender, EventArgs e)
        {
            FormVisualSettings form = new FormVisualSettings();
            form.Owner = this;
            form.Show();
        }

        private void glControlMain_MouseWheel(object sender, EventArgs e)
        {
            mainRenderer.ZoomActiveCamera();
        }

        private void stopAfterIterationscheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (stopAfterIterationscheckBox.Checked)
            {
                label3.Enabled = true;
                stopIterationsnumericUpDown.Enabled = true;
            }
            else
            {
                label3.Enabled = false;
                stopIterationsnumericUpDown.Enabled = false;
            }
        }

        private void glControlMain_Load(object sender, EventArgs e)
        {
            mainRenderer = new OpenGLRenderer();
            mainRenderer.Initialize(glControlMain);
            tickTimespan = new TimeSpan((long)(Math.Pow(10, 7) * Body.dt));
        }

        public static OpenGLRenderer GetCurrentRenderer()
        {
            if (mainRenderer == null)
                mainRenderer = new OpenGLRenderer();
            return mainRenderer;
        }

    }
}
