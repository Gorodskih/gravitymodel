﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;

namespace GravityModel
{
    public class OpenGLRenderer
    {

        bool loaded = false;
        GLControl window;
        ModelFromFile sfera;
        Lighting_technique mainShader;
        Pipeline mainPipe;
        OGLText xName, yName, zName;
        Axis standartAxis;
        Color axisColor;
        Camera mainCamera;
        public DirectionalLight directionalLight;
        public float FOV, viewDistance;

        public void Initialize(GLControl targetWindow)
        {
            window = targetWindow;
            mainPipe = new Pipeline();
            SetupViewport(1.12f, 10000);

            GL.ClearColor(Color.White);
            GL.Enable(EnableCap.DepthTest);

            Body.SetTailParams(1000, 50f);

            axisColor = Color.Black;
            SetAxises(2000, 100, 20f, new Font(FontFamily.GenericMonospace, 15f), new SolidBrush(axisColor));

            mainShader = new Lighting_technique();
            mainShader.Init();
            mainShader.Enable();
            mainShader.SetTextureUnit((int)TextureUnit.Texture0);

            directionalLight.Color = new Vector3(1.0f, 1.0f, 1.0f);
            directionalLight.AmbientIntensity = 0.25f;
            directionalLight.DiffuseIntensity = 0.75f;
            directionalLight.Direction = new Vector3(-1.0f, 0.0f, 0.0f);
            SetLight(directionalLight);

            Vector3 eye = new Vector3(200, 200, 200);
            Vector3 target = new Vector3(0);
            Vector3 up = new Vector3(0, 0, 1);
            mainCamera = new Camera(eye, target, up);
            mainPipe.SetActiveCamera(mainCamera);

            sfera = new ModelFromFile();
            sfera.LoadModel("sphere.stl");
            sfera.BindData();
            loaded = true;
        }

        public void SetupViewport(float _FOV, float _viewDistance)
        {
            FOV = _FOV;
            viewDistance = _viewDistance;

            if ((window.Height > 0) && (window.Width > 0))
            {
                float aspect = window.Width / window.Height;
                if (aspect <= 0) aspect = window.Height / window.Width;
                GL.Viewport(0, 0, window.Width, window.Height);
                mainPipe.SetProjectionMatrix(Matrix4.CreatePerspectiveFieldOfView(FOV, aspect, 1f, viewDistance));
            }
        }

        public void Render()
        {
            if (!loaded)
                return;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            DrawBodies(Form1.movingBodies);

            DrawTails(Form1.movingBodies);

            DrawAxises(standartAxis);

            window.SwapBuffers();

        }

        private void DrawBodies(List<Body> bodies)
        {
            mainShader.SetLightON(true);
            mainShader.SetTexON(false);

            mainPipe.SetRotationMatrix(Matrix4.Identity);
            foreach (Body body in bodies)
            {
                mainPipe.SetWorldPosition((float)body.position.X, (float)body.position.Y, (float)body.position.Z);
                mainPipe.SetScale((float)body.radius);
                mainShader.SetViewMatrix(mainPipe.ViewMatrix);
                mainShader.SetColor(body.color);
                mainShader.SetWVP(mainPipe.WVPMatrix);
                sfera.Render(mainShader.GetAttribs());
            }
        }

        private void DrawTails(List<Body> bodies)
        {
            mainShader.SetLightON(false);
            mainShader.SetTexON(false);

            mainPipe.SetScale(1);
            mainPipe.SetWorldPosition(0, 0, 0);
            mainPipe.SetRotationMatrix(Matrix4.Identity);
            mainShader.SetColor(new Color4(255, 0, 0, 255));
            mainShader.SetWVP(mainPipe.WVPMatrix);
            Attribs attrs = mainShader.GetAttribs();
            GL.BindBuffer(BufferTarget.ArrayBuffer, Body.tailVBO);
            GL.EnableVertexAttribArray(attrs.posAttrib);
            GL.VertexAttribPointer(attrs.posAttrib, 3, VertexAttribPointerType.Float, false, 0, 0);
            foreach (Body body in bodies)
            {
                if (body.Tail.Count > 1)
                {
                    Monitor.Enter(body.editingTail);
                    try
                    {
                        GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(body.Tail.Count * sizeof(float)), body.Tail.ToArray(), BufferUsageHint.StreamDraw);
                    }
                    finally
                    {
                        Monitor.Exit(body.editingTail);
                    }
                    GL.DrawArrays(PrimitiveType.LineStrip, 0, (int)(body.Tail.Count / 3));
                }
            }
            GL.DisableVertexAttribArray(attrs.posAttrib);
        }

        private void DrawAxises(Axis axis)
        {
            mainPipe.SetScale(1);
            mainShader.SetColor(new Color4(axisColor.R, axisColor.G, axisColor.B, axisColor.A));

            mainPipe.SetWorldPosition(0, 0, 0);
            mainShader.SetWVP(mainPipe.WVPMatrix);
            axis.Render(mainShader, mainPipe);

            mainPipe.SetWorldPosition(xName.Location);
            mainShader.SetWVP(mainPipe.WVPMatrix);
            xName.Render(mainShader);

            mainPipe.SetWorldPosition(0, 0, 0);
            mainPipe.SetRotationMatrix("z", 0, 0, (float)(90 * (Math.PI / 180)));
            mainShader.SetWVP(mainPipe.WVPMatrix);
            axis.Render(mainShader, mainPipe);

            mainPipe.SetWorldPosition(yName.Location);
            mainShader.SetWVP(mainPipe.WVPMatrix);
            yName.Render(mainShader);

            mainPipe.SetWorldPosition(0, 0, 0);
            mainPipe.SetRotationMatrix("y", 0, (float)(-90 * (Math.PI / 180)), 0);
            mainShader.SetWVP(mainPipe.WVPMatrix);
            axis.Render(mainShader, mainPipe);

            mainPipe.SetWorldPosition(zName.Location);
            mainShader.SetWVP(mainPipe.WVPMatrix);
            zName.Render(mainShader);
        }

        public void MoveActiveCamera()
        {
            mainPipe.ActiveCamera.Move();
        }

        public void ZoomActiveCamera()
        {
            mainPipe.ActiveCamera.Zoom();
        }

        public Camera GetActiveCamera()
        {
            return mainCamera;
        }

        public Axis GetMainAxis()
        {
            return standartAxis;
        }

        public void SetActiveCamera(Camera newCamera)
        {
            mainCamera = newCamera;
            mainPipe.SetActiveCamera(mainCamera);
        }

        public void SetLight(DirectionalLight light)
        {
            directionalLight = light;
            mainShader.SetDirectionalLight(directionalLight);
        }

        public void SetAxises(float length, float division, float divisionLength, Font textFont, SolidBrush textBrush)
        {
            standartAxis = new Axis();
            standartAxis.Set(length, division, divisionLength, textFont, textBrush);

            xName = new OGLText();
            xName.SetText("X", textFont, textBrush);
            xName.Location = new Vector3(division, -divisionLength, 0);

            yName = new OGLText();
            yName.SetText("Y", textFont, textBrush);
            yName.Location = new Vector3(division, -divisionLength, 0);

            zName = new OGLText();
            zName.SetText("Z", textFont, textBrush);
            zName.Location = new Vector3(division, -divisionLength, 0);
        }

        public void SetAxisColor(Color color)
        {
            axisColor = color;
            standartAxis.Set(standartAxis.length, standartAxis.division, standartAxis.divisionLength, standartAxis.font, new SolidBrush(axisColor));
        }

        public void SetClearColor(Color color)
        {
            GL.ClearColor(color);
        }
    }
}
