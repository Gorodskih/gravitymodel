﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;

namespace GravityModel
{

    public struct Attribs
    {
        public int
            posAttrib,
            texAttrib,
            normAttrib,
            texSamplerPos;
    }

    public struct DirectionalLight
    {
        public Vector3 Color;
        public float AmbientIntensity;
        public Vector3 Direction;
        public float DiffuseIntensity;
    };

    public class Lighting_technique : Technique
    {
        static string pVS = @"                                                          
        #version 330
 
        in vec3 Position;
        in vec2 TexCoord;
        in vec3 Normal;
 
        uniform mat4 gWVP;
        uniform mat4 gView;
 
        out vec2 TexCoord0;
        out vec3 Normal0;
 
        void main()
        {
            gl_Position = gWVP * vec4(Position, 1.0);
            TexCoord0 = TexCoord;
            Normal0 = (gView * vec4(Normal, 0.0)).xyz;
        }";

        static string pFS = @"                                                          
        #version 330                                                                        
                                                                                            
        in vec2 TexCoord0;                                                                  
        in vec3 Normal0;                                                                    
                                                                                            
        out vec4 FragColor;                                                                 
                                                                                            
        struct DirectionalLight                                                             
        {                                                                                   
            vec3 Color;                                                                     
            float AmbientIntensity;                                                         
            vec3 Direction;                                                                 
            float DiffuseIntensity;                                                         
        };                                                                                  
                                                                                            
        uniform DirectionalLight gDirectionalLight;                                         
        uniform sampler2D gSampler;                                                         
         
        uniform bool LightON;
        uniform bool TexON;
        uniform vec4 Color; 
                                                                                   
        void main()                                                                         
        {      

            vec4 AmbientColor = vec4(1.0f,1.0f,1.0f,1.0f);
            vec4 DiffuseColor = vec4(0,0,0,0);

            if (LightON){                                                                             
                AmbientColor = vec4(gDirectionalLight.Color, 1.0f) * gDirectionalLight.AmbientIntensity;                         
                                                                                            
                float DiffuseFactor = dot(normalize(Normal0), -gDirectionalLight.Direction);                                                                                                                                                            
                                                                                            
                if (DiffuseFactor > 0){                                                         
                    DiffuseColor = vec4(gDirectionalLight.Color, 1.0f) * gDirectionalLight.DiffuseIntensity * DiffuseFactor;                                               
                }                                                                               
                else{                                                                           
                    DiffuseColor = vec4(0,0,0,0);                                               
                }                                                                               
            }
            
            if (TexON){
                FragColor = texture2D(gSampler, TexCoord0.xy) * (AmbientColor + DiffuseColor); 
            }
            else{
                FragColor = Color * (AmbientColor + DiffuseColor);
            }                                    
        }";


        private int gWVPLocation,
                    m_ViewMatrixLocation,
                    lightONLocation,
                    TexONLocation,
                    colorLocation;
        public int m_samplerLocation;

        private Attribs m_attribs;


        struct LightLocation
        {
            public int Color;
            public int AmbientIntensity;
            public int Direction;
            public int DiffuseIntensity;
        }
        private LightLocation m_dirLightLocation;

        public Lighting_technique() { }

        public override bool Init()
        {
            if (!base.Init())
            {
                return false;
            }

            if (!AddShader(pVS, ShaderType.VertexShader))
            {
                return false;
            }

            if (!AddShader(pFS, ShaderType.FragmentShader))
            {
                return false;
            }

            if (!LinkAndValidate())
            {
                return false;
            }

            gWVPLocation = GetUniformLocation("gWVP");
            m_ViewMatrixLocation = GetUniformLocation("gView");
            m_samplerLocation = GetUniformLocation("gSampler");
            m_dirLightLocation.Color = GetUniformLocation("gDirectionalLight.Color");
            m_dirLightLocation.AmbientIntensity = GetUniformLocation("gDirectionalLight.AmbientIntensity");
            m_dirLightLocation.Direction = GetUniformLocation("gDirectionalLight.Direction");
            m_dirLightLocation.DiffuseIntensity = GetUniformLocation("gDirectionalLight.DiffuseIntensity");
            lightONLocation = GetUniformLocation("LightON");
            TexONLocation = GetUniformLocation("TexON");
            colorLocation = GetUniformLocation("Color");

            m_attribs.posAttrib = GetAttribLocation("Position");
            m_attribs.texAttrib = GetAttribLocation("TexCoord");
            m_attribs.normAttrib = GetAttribLocation("Normal");

            return true;
        }

        public Attribs GetAttribs()
        {
            return m_attribs;
        }

        public void SetViewMatrix(Matrix4 ViewMatrix)
        {
            GL.UniformMatrix4(m_ViewMatrixLocation, false, ref ViewMatrix);
        }

        public void SetWVP(Matrix4 WVP)
        {
            GL.UniformMatrix4(gWVPLocation, false, ref WVP);
        }

        public void SetTextureUnit(int TextureUnit)
        {
            GL.Uniform1(m_samplerLocation, TextureUnit);
        }

        public void SetDirectionalLight(DirectionalLight Light)
        {
            GL.Uniform3(m_dirLightLocation.Color, Light.Color.X, Light.Color.Y, Light.Color.Z);
            GL.Uniform1(m_dirLightLocation.AmbientIntensity, Light.AmbientIntensity);
            Vector3 Direction = Light.Direction;
            Direction.Normalize();
            GL.Uniform3(m_dirLightLocation.Direction, Direction.X, Direction.Y, Direction.Z);
            GL.Uniform1(m_dirLightLocation.DiffuseIntensity, Light.DiffuseIntensity);
        }

        public void SetLightON(bool ON)
        {
            GL.Uniform1(lightONLocation, Convert.ToInt32(ON));
        }

        public void SetTexON(bool ON)
        {
            GL.Uniform1(TexONLocation, Convert.ToInt32(ON));
        }

        public void SetColor(Color4 color)
        {
            GL.Uniform4(colorLocation, color);
        }
    }
}