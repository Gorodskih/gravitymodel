﻿namespace GravityModel
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.glControlMain = new OpenTK.GLControl();
            this.renderTimer = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.stopIterationsnumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.stopAfterIterationscheckBox = new System.Windows.Forms.CheckBox();
            this.checkBoxRefresh = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonStartStop = new System.Windows.Forms.Button();
            this.buttonEditBodies = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.visualSettingsButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stopIterationsnumericUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // glControlMain
            // 
            this.glControlMain.BackColor = System.Drawing.Color.Black;
            this.glControlMain.Location = new System.Drawing.Point(12, 12);
            this.glControlMain.Name = "glControlMain";
            this.glControlMain.Size = new System.Drawing.Size(1034, 657);
            this.glControlMain.TabIndex = 0;
            this.glControlMain.VSync = false;
            this.glControlMain.Load += new System.EventHandler(this.glControlMain_Load);
            this.glControlMain.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControlMain_MouseMove);
            // 
            // renderTimer
            // 
            this.renderTimer.Enabled = true;
            this.renderTimer.Interval = 15;
            this.renderTimer.Tick += new System.EventHandler(this.renderTimer_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.stopIterationsnumericUpDown);
            this.groupBox1.Controls.Add(this.stopAfterIterationscheckBox);
            this.groupBox1.Controls.Add(this.checkBoxRefresh);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.buttonStartStop);
            this.groupBox1.Location = new System.Drawing.Point(1052, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 228);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Моделирование движения";
            // 
            // label3
            // 
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(128, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "итераций.";
            // 
            // stopIterationsnumericUpDown
            // 
            this.stopIterationsnumericUpDown.Enabled = false;
            this.stopIterationsnumericUpDown.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.stopIterationsnumericUpDown.Location = new System.Drawing.Point(9, 192);
            this.stopIterationsnumericUpDown.Maximum = new decimal(new int[] {
            1241513983,
            370409800,
            542101,
            0});
            this.stopIterationsnumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.stopIterationsnumericUpDown.Name = "stopIterationsnumericUpDown";
            this.stopIterationsnumericUpDown.Size = new System.Drawing.Size(102, 20);
            this.stopIterationsnumericUpDown.TabIndex = 12;
            this.stopIterationsnumericUpDown.ThousandsSeparator = true;
            this.stopIterationsnumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // stopAfterIterationscheckBox
            // 
            this.stopAfterIterationscheckBox.AutoSize = true;
            this.stopAfterIterationscheckBox.Location = new System.Drawing.Point(7, 169);
            this.stopAfterIterationscheckBox.Name = "stopAfterIterationscheckBox";
            this.stopAfterIterationscheckBox.Size = new System.Drawing.Size(119, 17);
            this.stopAfterIterationscheckBox.TabIndex = 9;
            this.stopAfterIterationscheckBox.Text = "Остановить после";
            this.stopAfterIterationscheckBox.UseVisualStyleBackColor = true;
            this.stopAfterIterationscheckBox.CheckedChanged += new System.EventHandler(this.stopAfterIterationscheckBox_CheckedChanged);
            // 
            // checkBoxRefresh
            // 
            this.checkBoxRefresh.AutoSize = true;
            this.checkBoxRefresh.Checked = true;
            this.checkBoxRefresh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRefresh.Location = new System.Drawing.Point(7, 69);
            this.checkBoxRefresh.Name = "checkBoxRefresh";
            this.checkBoxRefresh.Size = new System.Drawing.Size(116, 30);
            this.checkBoxRefresh.TabIndex = 6;
            this.checkBoxRefresh.Text = "Обновить модель\r\nиз таблицы";
            this.checkBoxRefresh.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Прошедшее время:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "00:00:00";
            // 
            // buttonStartStop
            // 
            this.buttonStartStop.Location = new System.Drawing.Point(12, 19);
            this.buttonStartStop.Name = "buttonStartStop";
            this.buttonStartStop.Size = new System.Drawing.Size(117, 45);
            this.buttonStartStop.TabIndex = 0;
            this.buttonStartStop.Text = "Запуск";
            this.buttonStartStop.UseVisualStyleBackColor = true;
            this.buttonStartStop.Click += new System.EventHandler(this.buttonStartStop_Click);
            // 
            // buttonEditBodies
            // 
            this.buttonEditBodies.Location = new System.Drawing.Point(12, 19);
            this.buttonEditBodies.Name = "buttonEditBodies";
            this.buttonEditBodies.Size = new System.Drawing.Size(117, 45);
            this.buttonEditBodies.TabIndex = 2;
            this.buttonEditBodies.Text = "Редактировать";
            this.buttonEditBodies.UseVisualStyleBackColor = true;
            this.buttonEditBodies.Click += new System.EventHandler(this.buttonEditBodies_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonEditBodies);
            this.groupBox2.Location = new System.Drawing.Point(1052, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 78);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Список тел";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.visualSettingsButton);
            this.groupBox3.Location = new System.Drawing.Point(1052, 330);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 99);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Визуализация";
            // 
            // visualSettingsButton
            // 
            this.visualSettingsButton.Location = new System.Drawing.Point(12, 28);
            this.visualSettingsButton.Name = "visualSettingsButton";
            this.visualSettingsButton.Size = new System.Drawing.Size(117, 55);
            this.visualSettingsButton.TabIndex = 0;
            this.visualSettingsButton.Text = "Настройки";
            this.visualSettingsButton.UseVisualStyleBackColor = true;
            this.visualSettingsButton.Click += new System.EventHandler(this.visualSettingsButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.glControlMain);
            this.Name = "Form1";
            this.Text = "Решение задачи N тел";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stopIterationsnumericUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private OpenTK.GLControl glControlMain;
        private System.Windows.Forms.Timer renderTimer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonStartStop;
        private System.Windows.Forms.CheckBox checkBoxRefresh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonEditBodies;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button visualSettingsButton;
        private System.Windows.Forms.CheckBox stopAfterIterationscheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown stopIterationsnumericUpDown;
    }
}

