﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GravityModel
{
    public class Axis
    {
        int VBO;
        int verticesCount;
        OGLText[] signs;
        public float length, division, divisionLength;
        public Font font;
        public SolidBrush brush;

        public Axis()
        {
            VBO = GL.GenBuffer();
        }

        public void Set(float _length, float _division, float _divisionLength, Font textFont, SolidBrush textBrush)
        {
            length = _length;
            division = _division;
            divisionLength = _divisionLength;
            font = textFont;
            brush = textBrush;

            List<float> VerticesList = new List<float>();
            VerticesList.AddRange(new float[6] { -length, 0, 0, length, 0, 0 });
            float lastDivision = (int)(length / division) * division;
            List<OGLText> signsList = new List<OGLText>();

            for (float i = -lastDivision; i <= lastDivision; i += division)
            {
                VerticesList.AddRange(new float[6] { i, -divisionLength / 2, 0, i, divisionLength / 2, 0 });
                if (i != 0)
                {
                    OGLText currSign = new OGLText();
                    currSign.SetText(i.ToString(), font, brush);
                    currSign.Location = new Vector3(i, divisionLength, 0);
                    signsList.Add(currSign);
                }
            }
            signs = signsList.ToArray();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(VerticesList.Count * sizeof(float)), VerticesList.ToArray(), BufferUsageHint.StaticDraw);

            verticesCount = VerticesList.Count;
        }

        public void Render(Lighting_technique shaderPrograms, Pipeline pipe)
        {
            shaderPrograms.SetLightON(false);
            shaderPrograms.SetTexON(false);
            Attribs attrs = shaderPrograms.GetAttribs();
            GL.EnableVertexAttribArray(attrs.posAttrib);
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.VertexAttribPointer(attrs.posAttrib, 3, VertexAttribPointerType.Float, false, 0, 0);
            GL.DrawArrays(PrimitiveType.Lines, 0, verticesCount);
            GL.DisableVertexAttribArray(attrs.posAttrib);
            shaderPrograms.SetTexON(true);
            foreach (OGLText text in signs)
            {
                pipe.SetWorldPosition(text.Location);
                shaderPrograms.SetWVP(pipe.WVPMatrix);
                text.Render(shaderPrograms);
            }
        }
    }
}
