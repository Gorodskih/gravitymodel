﻿using Assimp;
using Assimp.Configs;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GravityModel
{

    class ModelFromFile
    {
        Scene model;
        List<int> VBList;
        List<int> IBList;
        List<Texture> Texs;
        List<int> indicesCount;

        public ModelFromFile()
        {
            VBList = new List<int>();
            IBList = new List<int>();
            Texs = new List<Texture>();
            indicesCount = new List<int>();
        }

        public void LoadModel(string FileName)
        {
            Clear();
            AssimpContext Importer = new AssimpContext();
            NormalSmoothingAngleConfig config = new NormalSmoothingAngleConfig(66.0f);
            Importer.SetConfig(config);
            try
            {
                model = Importer.ImportFile(FileName, PostProcessSteps.Triangulate | PostProcessSteps.GenerateSmoothNormals);
            }
            catch
            {
                MessageBox.Show("Невозможно загрузить модель " + FileName, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

        }

        public void LoadModel(string FileName, string[] textureNames)
        {
            LoadModel(FileName);
            foreach (string textura in textureNames)
            {
                Texture tex = new Texture(TextureTarget.Texture2D);
                tex.Load(textura);
                Texs.Add(tex);
            }
        }

        public void BindData()
        {
            foreach (Mesh mesh in model.Meshes)
            {

                int VBO, IBO;

                float[] Vertices = new float[mesh.VertexCount * 8];
                Vertices.Initialize();
                int j = 0;
                for (int i = 0; i < mesh.VertexCount; i++)
                {
                    Vertices[j] = mesh.Vertices[i].X;
                    Vertices[j + 1] = mesh.Vertices[i].Y;
                    Vertices[j + 2] = mesh.Vertices[i].Z;
                    if (mesh.TextureCoordinateChannelCount > 0)
                    {
                        Vertices[j + 3] = mesh.TextureCoordinateChannels[0][i].X;
                        Vertices[j + 4] = mesh.TextureCoordinateChannels[0][i].Y;
                    }
                    if (mesh.HasNormals)
                    {
                        Vertices[j + 5] = mesh.Normals[i].X;
                        Vertices[j + 6] = mesh.Normals[i].Y;
                        Vertices[j + 7] = mesh.Normals[i].Z;
                    }
                    j += 8;
                }


                VBO = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vertices.Length * sizeof(float)), Vertices, BufferUsageHint.StaticDraw);

                int[] Indices = mesh.GetIndices();
                indicesCount.Add(Indices.Length);
                IBO = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, IBO);
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(Indices.Length * sizeof(int)), Indices, BufferUsageHint.StaticDraw);

                VBList.Add(VBO);
                IBList.Add(IBO);
            }

        }

        public void Render(Attribs attrs)
        {
            GL.EnableVertexAttribArray(attrs.posAttrib);
            GL.EnableVertexAttribArray(attrs.texAttrib);
            GL.EnableVertexAttribArray(attrs.normAttrib);

            for (int i = 0; i < model.MeshCount; i++)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, VBList.ElementAt(i));
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, IBList.ElementAt(i));

                GL.VertexAttribPointer(attrs.posAttrib, 3, VertexAttribPointerType.Float, false, 8 * sizeof(float), 0);
                GL.VertexAttribPointer(attrs.texAttrib, 2, VertexAttribPointerType.Float, false, 8 * sizeof(float), 3 * sizeof(float));
                GL.VertexAttribPointer(attrs.normAttrib, 3, VertexAttribPointerType.Float, false, 8 * sizeof(float), 5 * sizeof(float));

                if (i < Texs.Count)
                    Texs.ElementAt(i).Bind();

                GL.DrawElements(BeginMode.Triangles, indicesCount.ElementAt(i), DrawElementsType.UnsignedInt, 0);
            }

            GL.DisableVertexAttribArray(attrs.posAttrib);
            GL.DisableVertexAttribArray(attrs.texAttrib);
            GL.DisableVertexAttribArray(attrs.normAttrib);

        }

        public void Clear()
        {
            GL.DeleteBuffers(VBList.Count, VBList.ToArray());
            VBList.Clear();
            GL.DeleteBuffers(IBList.Count, IBList.ToArray());
            IBList.Clear();
            Texs.Clear();
            indicesCount.Clear();
        }
    }
}