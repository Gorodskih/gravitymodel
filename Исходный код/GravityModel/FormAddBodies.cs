﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GravityModel
{
    public partial class FormAddBodies : Form
    {
        public FormAddBodies()
        {
            InitializeComponent();
        }

        private void updatedataGridView()
        {
            foreach (Body body in Form1.loadedBodies)
            {
                dataGridView1.Rows.Add(
                    body.tracking,
                    body.radius,
                    body.mass,
                    body.position.X, body.position.Y, body.position.Z,
                    body.speed.X, body.speed.Y, body.speed.Z
                    );
            }
            for (int i = 0; i < Form1.loadedBodies.Count; i++)
                dataGridView1.Rows[i].Cells["colorColumn"].Style.BackColor = Color.FromArgb(Form1.loadedBodies.ElementAt(i).color.ToArgb());          
            numericUpDowndt.Value = (decimal)Body.dt;
            numericUpDownG.Value = (decimal)(Body.G * Math.Pow(10, -Body.Gpower));
            numericUpDownGpower.Value = (decimal)Body.Gpower;
        }

        private void FormAddBodies_Shown(object sender, EventArgs e)
        {
            updatedataGridView();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            List<Body> temp = new List<Body>();
            try
            {
                Body.G = (double)numericUpDownG.Value * Math.Pow(10, (double)numericUpDownGpower.Value);
                Body.Gpower = (int)numericUpDownGpower.Value;
                Body.dt = (double)numericUpDowndt.Value;
                Form1.tickTimespan = new TimeSpan((long)(Math.Pow(10, 7) * Body.dt));
            }
            catch
            {
                MessageBox.Show("Некорректно введена гравитационная постоянная, либо dt.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            int currentRow = 1;
            try {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                    if ((Single.Parse(row.Cells["massColumn"].Value.ToString()) <= 0) || (Single.Parse(row.Cells["radiusColumn"].Value.ToString()) <= 0))
                        throw new Exception();
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    Color color = row.Cells["colorColumn"].Style.BackColor;
                    if (color.IsEmpty)
                        color = Color.Blue;
                    temp.Add(new Body(
                        (bool)row.Cells["trackColumn"].FormattedValue,
                        Single.Parse(row.Cells["radiusColumn"].Value.ToString()),
                        Single.Parse(row.Cells["massColumn"].Value.ToString()),
                        new Vector3d(
                        Single.Parse(row.Cells["xColumn"].Value.ToString()),
                        Single.Parse(row.Cells["YColumn"].Value.ToString()),
                        Single.Parse(row.Cells["ZColumn"].Value.ToString())
                            ),
                        new Vector3d(
                            Single.Parse(row.Cells["speedXColumn"].Value.ToString()),
                            Single.Parse(row.Cells["speedYColumn"].Value.ToString()),
                            Single.Parse(row.Cells["speedZColumn"].Value.ToString())
                            ),
                        new Color4(color.R, color.G, color.B, color.A)
                        ));
                    currentRow++;
                }
            }
            catch
            {
                MessageBox.Show("Некорректные данные либо пустое поле в строке " + currentRow.ToString() + ".", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Form1.loadedBodies = temp;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Add();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Remove(dataGridView1.SelectedRows[0]);
        }

        private void buttonPresets_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(buttonPresets, 0, 0);
        }

        private void СтолкновениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Rows.Add(true, 10, 10, -506, 10, 0, 2, -2, 0);
            dataGridView1.Rows.Add(true, 10, 10, 506, 10, 506, -2, 2, 0);
            dataGridView1.Rows.Add(true, 10, 10, 100, -590, 0, -1.5f, 1, 0);
            dataGridView1.Rows.Add(true, 10, 10, 0, -290, 0, 3, 3, 0);
            dataGridView1.Rows.Add(true, 50, 70000000000000, 0, 0, 0, 0, 0, 0);
            dataGridView1.Rows.Add(true, 30, 100000000000000, 1010, 110, 0, -1f, 0, 0);
            numericUpDownG.Value = 6.67M;
            numericUpDownGpower.Value = -11M;
        }

        private void устойчиваяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Rows.Add(true, 10, 10, -506, 10, 0, 2, -2, 0);
            dataGridView1.Rows.Add(true, 10, 10, 506, 10, 506, -2, 2, 0);
            dataGridView1.Rows.Add(true, 10, 10, 100, -590, 0, -1.5f, 1, 0);
            dataGridView1.Rows.Add(true, 10, 10, 0, -290, 0, 3, 3, 0);
            dataGridView1.Rows.Add(true, 50, 70000000000000, 0, 0, 0, 0, 0, 0);
            //dataGridView1.Rows.Add(true, 6.96, 1990000.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
            //dataGridView1.Rows.Add(true, 0.0249, 0.31, -15.296672429999999, -694.04901955000003, -55.187994639999999, 3.3509922880031997, 0.17951744396159999, -0.29919573996479998);
            //dataGridView1.Rows.Add(true, 0.0619, 4.9, 802.09607217999996, -727.35090264999997, -56.133324520000002, 2.0158312982687998, 2.2477079967263998, -0.104718508992);
            //dataGridView1.Rows.Add(true, 0.06378, 5.98, -260.28709927, 1449.4007748700001, 0.0, -2.5707675889727999, -0.47828234211840004, 0.0);
            //dataGridView1.Rows.Add(true, 0.0342, 0.65, -967.42668641, -2028.64285984, -18.698536969999999, 1.9891430381952, -0.71519749692479995, -0.059599791446400006);
            //dataGridView1.Rows.Add(true, 0.0045, 8.7 * Math.Pow(10, -5), 4250.6738781800004, -1029.2333455999999, -815.45798936999995, 0.3066756335136, 1.4062199780160001, -0.022439680473600003);
            //dataGridView1.Rows.Add(true, 0.005, 0.000318, 2778.1820437699998, -3633.2834686900001, 2283.61148555, 1.0247454095327999, 0.50863275803519992, -0.42635392951679996);
            //dataGridView1.Rows.Add(true, 0.0012, 2 * Math.Pow(10, -5), -4364.3682593800004, -219.01128168, 221.10565185999999, -0.2692761660288, -1.3688205105311999, 0.3066756335136);
            //dataGridView1.Rows.Add(true, 0.00265, 0.0003, -3065.5595520400002, -1206.50682155, 408.85097870999999, 0.75546924350400002, -1.5932173156128002, -0.052359254495999999);
            numericUpDownG.Value = 6.67M;
            numericUpDownGpower.Value = -11M;
        }

        private void невероятнаяВосьмеркаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Rows.Add(true, 0.1, 1, 0, 0, 0, -0.93240737f, -0.86473146f, 0);
            dataGridView1.Rows.Add(true, 0.1, 1, 0.97000436f, -0.24308753f, 0, 0.466203685f, 0.43236573f, 0);
            dataGridView1.Rows.Add(true, 0.1, 1, -0.97000436f, 0.24308753f, 0, 0.466203685f, 0.43236573f, 0);
            numericUpDownG.Value = 1M;
            numericUpDownGpower.Value = 0M;
            numericUpDowndt.Value = 0.0001M;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex == 9))
            {
                if (colorDialog1.ShowDialog() == DialogResult.OK)
                {
                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = colorDialog1.Color;
                }
            }
        }
    }
}
