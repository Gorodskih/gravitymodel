﻿using Assimp;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace GravityModel
{
    class Texture
    {
        private TextureTarget textureTarget;
        private string fileName;
        public int textureObject;

        public Texture(TextureTarget _textureTarget)
        {
            textureTarget = _textureTarget;
        }

        public void Load(string _fileName)
        {
            fileName = _fileName;
            if (String.IsNullOrEmpty(fileName))
                throw new ArgumentException(fileName);

            GL.GenTextures(1, out textureObject);
            GL.BindTexture(TextureTarget.Texture2D, textureObject);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            Bitmap bmp = new Bitmap(Image.FromFile(fileName));
            BitmapData bmp_data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmp_data.Width, bmp_data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmp_data.Scan0);

            bmp.UnlockBits(bmp_data);
        }

       public void LoadFromEmbedded(EmbeddedTexture texture)
        {
            GL.GenTextures(1, out textureObject);
            GL.BindTexture(TextureTarget.Texture2D, textureObject);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, texture.Width, texture.Height, 0,
            OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, texture.NonCompressedData);
        }

        public void Bind()
        {
            GL.BindTexture(textureTarget, textureObject);
        }
    }
}
