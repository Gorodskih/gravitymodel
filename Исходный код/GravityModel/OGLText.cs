﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace GravityModel
{
    class OGLText
    {
        
        int text_texture;
        static int VBO = GL.GenBuffer();
        public Vector3 Location;
        float[] Vertices;

        public OGLText()
        {          
            text_texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, text_texture);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)All.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)All.Linear);            
        }

        public void SetText(string text, Font font, Brush brush)
        {
            float x = text.Length * font.SizeInPoints;
            float y = font.SizeInPoints * 1.5f;
            Vertices = new float[]{
               -x,  0, 0,      1,0,
                0,  0, 0,      0,0,
                0,  y, 0,      0,1,
               -x,  y, 0,      1,1
            };

            Bitmap text_bmp = new Bitmap((int)(text.Length * font.SizeInPoints), (int)y);
            using (Graphics gfx = Graphics.FromImage(text_bmp))
            {
                gfx.Clear(Color.Transparent);
                gfx.DrawString(text, font, brush, new RectangleF(0, 0, text_bmp.Width, text_bmp.Height));
            }

            GL.BindTexture(TextureTarget.Texture2D, text_texture);
            BitmapData data = text_bmp.LockBits(new Rectangle(0, 0, text_bmp.Width, text_bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            text_bmp.UnlockBits(data);

        }

        public void Render(Lighting_technique shader)
        {
            Attribs attr = shader.GetAttribs();
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.OneMinusSrcAlpha);
            GL.EnableVertexAttribArray(attr.posAttrib);
            GL.EnableVertexAttribArray(attr.texAttrib);

            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vertices.Length * sizeof(float)), Vertices, BufferUsageHint.StreamDraw);
            GL.VertexAttribPointer(attr.posAttrib, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);
            GL.VertexAttribPointer(attr.texAttrib, 2, VertexAttribPointerType.Float, false, 5 * sizeof(float), 3 * sizeof(int));

            GL.BindTexture(TextureTarget.Texture2D, text_texture);
            GL.DrawArrays(PrimitiveType.Quads, 0, 4);

            GL.DisableVertexAttribArray(attr.posAttrib);
            GL.DisableVertexAttribArray(attr.texAttrib);
            GL.Disable(EnableCap.Blend);
        }

    }
}
