﻿using OpenTK.Input;
using OpenTK;

namespace GravityModel
{
    public class Camera
    {

        public Matrix4 ViewMatrix;
        public Vector3 eye, up, ViewVector;
        public Vector3 target
        {
            get { return eye + ViewVector; }
        }
        private int prevX = 0,
            prevY = 0,
            prevWheel = 0;
        public float rotateSpeedModule = 0.01f,
            zoomSpeedModule = 10f,
            translateSpeedModule = 1f;


        public Camera()
        {
            eye = new Vector3(0, 0, 1);
            up = new Vector3(0, 1, 0);
            ViewVector = new Vector3(0, 0, -1);
            ViewMatrix = Matrix4.LookAt(eye, target, up);
        }

        public Camera(Vector3 _eye, Vector3 _target, Vector3 _up)
        {
            eye = _eye;
            up = _up.Normalized();
            ViewVector = _target - eye;
            ViewMatrix = Matrix4.LookAt(eye, target, up);
        }

        public void Move()
        {
            MouseState mouse = Mouse.GetCursorState();
            if (mouse[MouseButton.Middle])
            {
                if (mouse.X != prevX)
                {
                    float speedStep = rotateSpeedModule;
                    if (mouse.X > prevX)
                        speedStep *= -1;
                    Matrix4 rotation = Matrix4.CreateFromAxisAngle(up, speedStep);
                    Vector3.Transform(ref ViewVector, ref rotation, out ViewVector);
                }
                if (mouse.Y != prevY)
                {
                    Vector3 rotateVector = Vector3.Cross(ViewVector, up);
                    float speedStep = rotateSpeedModule;
                    if (mouse.Y > prevY)
                        speedStep *= -1;
                    Matrix4 rotation = Matrix4.CreateFromAxisAngle(rotateVector, speedStep);
                    Vector3.Transform(ref ViewVector, ref rotation, out ViewVector);
                    Vector3.Transform(ref up, ref rotation, out up);
                }
                ViewMatrix = Matrix4.LookAt(eye, target, up);
            }
            if (mouse[MouseButton.Right])
            {
                if (mouse.X != prevX)
                {
                    float speedStep = translateSpeedModule;
                    if (mouse.X > prevX)
                        speedStep *= -1;
                    eye += Vector3.Cross(ViewVector, up).Normalized() * speedStep;
                }
                if (mouse.Y != prevY)
                {
                    float speedStep = translateSpeedModule;
                    if (mouse.Y < prevY)
                        speedStep *= -1;
                    eye += up * speedStep;
                }
                ViewMatrix = Matrix4.LookAt(eye, target, up);
            }
            if (mouse[MouseButton.Left])
            {
                if (mouse.X != prevX)
                {
                    float speedStep = rotateSpeedModule;
                    if (mouse.X > prevX)
                        speedStep *= -1;
                    Matrix4 rotation = Matrix4.CreateFromAxisAngle(up, speedStep);
                    Vector3.Transform(ref ViewVector, ref rotation, out ViewVector);
                    Vector3.Transform(ref eye, ref rotation, out eye);
                }
                if (mouse.Y != prevY)
                {
                    Vector3 rotateVector = Vector3.Cross(ViewVector, up);
                    float speedStep = rotateSpeedModule;
                    if (mouse.Y > prevY)
                        speedStep *= -1;
                    Matrix4 rotation = Matrix4.CreateFromAxisAngle(rotateVector, speedStep);
                    Vector3.Transform(ref ViewVector, ref rotation, out ViewVector);
                    Vector3.Transform(ref up, ref rotation, out up);
                    Vector3.Transform(ref eye, ref rotation, out eye);
                }
                ViewMatrix = Matrix4.LookAt(eye, target, up);
            }

            prevX = mouse.X;
            prevY = mouse.Y;
        }

        public void Zoom()
        {
            MouseState mouse = Mouse.GetState(0);
            float speedStep = zoomSpeedModule;
            if (mouse.Wheel < prevWheel)
                speedStep *= -1;
            eye += ViewVector.Normalized() * speedStep;
            ViewMatrix = Matrix4.LookAt(eye, target, up);
            prevWheel = mouse.Wheel;
        }

        public void SetSpeed(float rotate, float translate, float zoom)
        {
            rotateSpeedModule = rotate;
            zoomSpeedModule = zoom;
            translateSpeedModule = translate;
        }

    }
}