﻿namespace GravityModel
{
    partial class FormAddBodies
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.trackColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.radiusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.massColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speedXColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speedYColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speedZColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonPresets = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.предустановка1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.устойчиваяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.невероятнаяВосьмеркаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownG = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownGpower = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDowndt = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGpower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowndt)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.trackColumn,
            this.radiusColumn,
            this.massColumn,
            this.xColumn,
            this.yColumn,
            this.zColumn,
            this.speedXColumn,
            this.speedYColumn,
            this.speedZColumn,
            this.colorColumn});
            this.dataGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(734, 402);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // trackColumn
            // 
            this.trackColumn.FalseValue = "False";
            this.trackColumn.HeaderText = "Траектория";
            this.trackColumn.Name = "trackColumn";
            this.trackColumn.TrueValue = "True";
            this.trackColumn.Width = 73;
            // 
            // radiusColumn
            // 
            this.radiusColumn.HeaderText = "Радиус (м)";
            this.radiusColumn.Name = "radiusColumn";
            this.radiusColumn.Width = 78;
            // 
            // massColumn
            // 
            this.massColumn.HeaderText = "Масса (кг)";
            this.massColumn.Name = "massColumn";
            this.massColumn.Width = 78;
            // 
            // xColumn
            // 
            this.xColumn.HeaderText = "X";
            this.xColumn.Name = "xColumn";
            this.xColumn.Width = 39;
            // 
            // yColumn
            // 
            this.yColumn.HeaderText = "Y";
            this.yColumn.Name = "yColumn";
            this.yColumn.Width = 39;
            // 
            // zColumn
            // 
            this.zColumn.HeaderText = "Z";
            this.zColumn.Name = "zColumn";
            this.zColumn.Width = 39;
            // 
            // speedXColumn
            // 
            this.speedXColumn.HeaderText = "Скорость по Х (м/c)";
            this.speedXColumn.Name = "speedXColumn";
            this.speedXColumn.Width = 90;
            // 
            // speedYColumn
            // 
            this.speedYColumn.HeaderText = "Скорость по Y (м/c)";
            this.speedYColumn.Name = "speedYColumn";
            this.speedYColumn.Width = 90;
            // 
            // speedZColumn
            // 
            this.speedZColumn.HeaderText = "Скорость по Z (м/c)";
            this.speedZColumn.Name = "speedZColumn";
            this.speedZColumn.Width = 90;
            // 
            // colorColumn
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Blue;
            this.colorColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.colorColumn.HeaderText = "Цвет";
            this.colorColumn.Name = "colorColumn";
            this.colorColumn.ReadOnly = true;
            this.colorColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colorColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colorColumn.Width = 38;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(760, 128);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(150, 35);
            this.buttonSave.TabIndex = 4;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(760, 12);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(150, 35);
            this.buttonAdd.TabIndex = 5;
            this.buttonAdd.Text = "Добавить строку";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(760, 54);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(150, 35);
            this.buttonDelete.TabIndex = 6;
            this.buttonDelete.Text = "Удалить строку";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonPresets
            // 
            this.buttonPresets.Location = new System.Drawing.Point(760, 375);
            this.buttonPresets.Name = "buttonPresets";
            this.buttonPresets.Size = new System.Drawing.Size(150, 35);
            this.buttonPresets.TabIndex = 7;
            this.buttonPresets.Text = "Предустановленные\r\nсистемы";
            this.buttonPresets.UseVisualStyleBackColor = true;
            this.buttonPresets.Click += new System.EventHandler(this.buttonPresets_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.предустановка1ToolStripMenuItem,
            this.устойчиваяToolStripMenuItem,
            this.невероятнаяВосьмеркаToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(218, 70);
            // 
            // предустановка1ToolStripMenuItem
            // 
            this.предустановка1ToolStripMenuItem.Name = "предустановка1ToolStripMenuItem";
            this.предустановка1ToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.предустановка1ToolStripMenuItem.Text = "Столкновение";
            this.предустановка1ToolStripMenuItem.Click += new System.EventHandler(this.СтолкновениеToolStripMenuItem_Click);
            // 
            // устойчиваяToolStripMenuItem
            // 
            this.устойчиваяToolStripMenuItem.Name = "устойчиваяToolStripMenuItem";
            this.устойчиваяToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.устойчиваяToolStripMenuItem.Text = "Устойчивая";
            this.устойчиваяToolStripMenuItem.Click += new System.EventHandler(this.устойчиваяToolStripMenuItem_Click);
            // 
            // невероятнаяВосьмеркаToolStripMenuItem
            // 
            this.невероятнаяВосьмеркаToolStripMenuItem.Name = "невероятнаяВосьмеркаToolStripMenuItem";
            this.невероятнаяВосьмеркаToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.невероятнаяВосьмеркаToolStripMenuItem.Text = "\"Невероятная восьмерка\"";
            this.невероятнаяВосьмеркаToolStripMenuItem.Click += new System.EventHandler(this.невероятнаяВосьмеркаToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(757, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 34);
            this.label1.TabIndex = 8;
            this.label1.Text = "Гравитационная\r\nпостоянная H*(м^2)/(кг^2)";
            // 
            // numericUpDownG
            // 
            this.numericUpDownG.DecimalPlaces = 3;
            this.numericUpDownG.Location = new System.Drawing.Point(760, 205);
            this.numericUpDownG.Maximum = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this.numericUpDownG.Minimum = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147483648});
            this.numericUpDownG.Name = "numericUpDownG";
            this.numericUpDownG.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownG.TabIndex = 9;
            this.numericUpDownG.Value = new decimal(new int[] {
            667,
            0,
            0,
            131072});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(835, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "*10^";
            // 
            // numericUpDownGpower
            // 
            this.numericUpDownGpower.Location = new System.Drawing.Point(878, 204);
            this.numericUpDownGpower.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDownGpower.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.numericUpDownGpower.Name = "numericUpDownGpower";
            this.numericUpDownGpower.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownGpower.TabIndex = 11;
            this.numericUpDownGpower.Value = new decimal(new int[] {
            11,
            0,
            0,
            -2147483648});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(757, 244);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "dt (c)";
            // 
            // numericUpDowndt
            // 
            this.numericUpDowndt.DecimalPlaces = 10;
            this.numericUpDowndt.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUpDowndt.Location = new System.Drawing.Point(760, 264);
            this.numericUpDowndt.Name = "numericUpDowndt";
            this.numericUpDowndt.Size = new System.Drawing.Size(150, 20);
            this.numericUpDowndt.TabIndex = 13;
            this.numericUpDowndt.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(760, 322);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 35);
            this.button1.TabIndex = 14;
            this.button1.Text = "Очистить таблицу";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormAddBodies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 426);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.numericUpDowndt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDownGpower);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownG);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonPresets);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormAddBodies";
            this.Text = "Список тел";
            this.Shown += new System.EventHandler(this.FormAddBodies_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGpower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDowndt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonPresets;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem предустановка1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem устойчиваяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem невероятнаяВосьмеркаToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownG;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownGpower;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDowndt;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn trackColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn radiusColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn massColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn speedXColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn speedYColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn speedZColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colorColumn;
    }
}