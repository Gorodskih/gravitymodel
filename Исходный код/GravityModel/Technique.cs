﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GravityModel
{
    public class Technique
    {
        public int shaderProgram;
        private List<int> shaderObjList;

        public Technique()
        {
            shaderObjList = new List<int>();
        }

        virtual public bool Init()
        {
            shaderProgram = GL.CreateProgram();
            if (shaderProgram == 0)
            {
                MessageBox.Show("Error creating shader program");
                return false;
            }
            return true;
        }

protected bool AddShader(string pShaderText, ShaderType type)
{
    int ShaderObj = GL.CreateShader(type);

    if (ShaderObj == 0)
    {
        MessageBox.Show("Error creating shader type " + type.ToString());
        return false;
    }

    shaderObjList.Add(ShaderObj);

    GL.ShaderSource(ShaderObj, pShaderText);
    GL.CompileShader(ShaderObj);
    int success;
    GL.GetShader(ShaderObj, ShaderParameter.CompileStatus, out success);
    if (success == 0)
    {
        String InfoLog;
        GL.GetShaderInfoLog(ShaderObj, out InfoLog);
        MessageBox.Show("Error compiling shader type " + type.ToString() + "\n" + InfoLog);
        return false;
    }

    GL.AttachShader(shaderProgram, ShaderObj);
    return true;
}

protected bool LinkAndValidate()
{
    int Success;
    String ErrorLog;

    GL.LinkProgram(shaderProgram);
    GL.GetProgram(shaderProgram, GetProgramParameterName.LinkStatus, out Success);
    if (Success == 0)
    {
        GL.GetProgramInfoLog(shaderProgram, out ErrorLog);
        MessageBox.Show("Error linking shader program: " + ErrorLog);
        return false;
    }

    GL.ValidateProgram(shaderProgram);
    GL.GetProgram(shaderProgram, GetProgramParameterName.ValidateStatus, out Success);
    if (Success == 0)
    {
        GL.GetProgramInfoLog(shaderProgram, out ErrorLog);
        MessageBox.Show("Invalid shader program: " + ErrorLog);
        return false;
    }

    foreach (int shaderObj in shaderObjList)
        GL.DeleteProgram(shaderObj);

    return true;
}

        protected int GetUniformLocation(string uniformName)
        {
            int Location = GL.GetUniformLocation(shaderProgram, uniformName);

            if (Location == int.MinValue)
            {
                MessageBox.Show("Warning! Unable to get the location of uniform " + uniformName);
            }

            return Location;
        }

        protected int GetAttribLocation(string attribName)
        {
            int Location = GL.GetAttribLocation(shaderProgram, attribName);

            if (Location == int.MinValue)
            {
                MessageBox.Show("Warning! Unable to get the location of uniform " + attribName);
            }

            return Location;
        }

        public void Enable()
        {
            GL.UseProgram(shaderProgram);
        }
    }
}
