﻿namespace GravityModel
{
    partial class FormVisualSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.saveTailsButton = new System.Windows.Forms.Button();
            this.tailNextPointAfterNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.tailLengthNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.axisFontSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.saveAxisesButton = new System.Windows.Forms.Button();
            this.axisDivisionLenghtNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.axisDivisionNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.axisLenghtNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lightSaveButton = new System.Windows.Forms.Button();
            this.ColorPanel = new System.Windows.Forms.Panel();
            this.changeColorButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.zLightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.yLightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.xLightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.diffuseTrack = new System.Windows.Forms.TrackBar();
            this.ambientTrack = new System.Windows.Forms.TrackBar();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.saveMouseSenseButton = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.cameraTranslateSpeedumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.cameraRotateSpeedNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.cameraZoomSpeedNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.SetCameraTargetButton = new System.Windows.Forms.Button();
            this.cameraTargetXnumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.cameraTargetYnumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cameraTargetZnumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.setCameraPositionButton = new System.Windows.Forms.Button();
            this.cameraPositionXnumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.cameraPositionYnumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cameraPositionZnumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.ViewSaveButton = new System.Windows.Forms.Button();
            this.viewDistNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.FOVnumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.setDarkButton = new System.Windows.Forms.Button();
            this.SetDarkCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tailNextPointAfterNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tailLengthNumericUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axisFontSizeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axisDivisionLenghtNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axisDivisionNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axisLenghtNumericUpDown)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zLightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yLightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xLightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diffuseTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ambientTrack)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraTranslateSpeedumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraRotateSpeedNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraZoomSpeedNumericUpDown)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraTargetXnumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraTargetYnumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraTargetZnumericUpDown)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraPositionXnumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraPositionYnumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraPositionZnumericUpDown)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewDistNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FOVnumericUpDown)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.saveTailsButton);
            this.groupBox1.Controls.Add(this.tailNextPointAfterNumericUpDown);
            this.groupBox1.Controls.Add(this.tailLengthNumericUpDown);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(268, 265);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(229, 184);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Отображение траектории тел";
            // 
            // saveTailsButton
            // 
            this.saveTailsButton.Location = new System.Drawing.Point(43, 147);
            this.saveTailsButton.Name = "saveTailsButton";
            this.saveTailsButton.Size = new System.Drawing.Size(123, 24);
            this.saveTailsButton.TabIndex = 5;
            this.saveTailsButton.Text = "Сохранить";
            this.saveTailsButton.UseVisualStyleBackColor = true;
            this.saveTailsButton.Click += new System.EventHandler(this.saveTailsButton_Click);
            // 
            // tailNextPointAfterNumericUpDown
            // 
            this.tailNextPointAfterNumericUpDown.DecimalPlaces = 2;
            this.tailNextPointAfterNumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.tailNextPointAfterNumericUpDown.Location = new System.Drawing.Point(43, 121);
            this.tailNextPointAfterNumericUpDown.Maximum = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.tailNextPointAfterNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.tailNextPointAfterNumericUpDown.Name = "tailNextPointAfterNumericUpDown";
            this.tailNextPointAfterNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.tailNextPointAfterNumericUpDown.TabIndex = 4;
            this.tailNextPointAfterNumericUpDown.ThousandsSeparator = true;
            this.tailNextPointAfterNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // tailLengthNumericUpDown
            // 
            this.tailLengthNumericUpDown.DecimalPlaces = 2;
            this.tailLengthNumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.tailLengthNumericUpDown.Location = new System.Drawing.Point(43, 49);
            this.tailLengthNumericUpDown.Maximum = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.tailLengthNumericUpDown.Name = "tailLengthNumericUpDown";
            this.tailLengthNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.tailLengthNumericUpDown.TabIndex = 3;
            this.tailLengthNumericUpDown.ThousandsSeparator = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Сохрание новой точки\r\nтраектории через (м):";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Длина траектории (м):";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.axisFontSizeNumericUpDown);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.saveAxisesButton);
            this.groupBox2.Controls.Add(this.axisDivisionLenghtNumericUpDown);
            this.groupBox2.Controls.Add(this.axisDivisionNumericUpDown);
            this.groupBox2.Controls.Add(this.axisLenghtNumericUpDown);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(268, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(229, 234);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Оси координат";
            // 
            // axisFontSizeNumericUpDown
            // 
            this.axisFontSizeNumericUpDown.Location = new System.Drawing.Point(45, 154);
            this.axisFontSizeNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.axisFontSizeNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.axisFontSizeNumericUpDown.Name = "axisFontSizeNumericUpDown";
            this.axisFontSizeNumericUpDown.Size = new System.Drawing.Size(116, 20);
            this.axisFontSizeNumericUpDown.TabIndex = 8;
            this.axisFontSizeNumericUpDown.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(43, 138);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(88, 13);
            this.label24.TabIndex = 7;
            this.label24.Text = "Размер шрифта";
            // 
            // saveAxisesButton
            // 
            this.saveAxisesButton.Location = new System.Drawing.Point(46, 183);
            this.saveAxisesButton.Name = "saveAxisesButton";
            this.saveAxisesButton.Size = new System.Drawing.Size(106, 22);
            this.saveAxisesButton.TabIndex = 6;
            this.saveAxisesButton.Text = "Сохранить";
            this.saveAxisesButton.UseVisualStyleBackColor = true;
            this.saveAxisesButton.Click += new System.EventHandler(this.saveAxisesButton_Click);
            // 
            // axisDivisionLenghtNumericUpDown
            // 
            this.axisDivisionLenghtNumericUpDown.DecimalPlaces = 2;
            this.axisDivisionLenghtNumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.axisDivisionLenghtNumericUpDown.Location = new System.Drawing.Point(46, 115);
            this.axisDivisionLenghtNumericUpDown.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.axisDivisionLenghtNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.axisDivisionLenghtNumericUpDown.Name = "axisDivisionLenghtNumericUpDown";
            this.axisDivisionLenghtNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.axisDivisionLenghtNumericUpDown.TabIndex = 5;
            this.axisDivisionLenghtNumericUpDown.ThousandsSeparator = true;
            this.axisDivisionLenghtNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // axisDivisionNumericUpDown
            // 
            this.axisDivisionNumericUpDown.DecimalPlaces = 2;
            this.axisDivisionNumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.axisDivisionNumericUpDown.Location = new System.Drawing.Point(46, 76);
            this.axisDivisionNumericUpDown.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.axisDivisionNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.axisDivisionNumericUpDown.Name = "axisDivisionNumericUpDown";
            this.axisDivisionNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.axisDivisionNumericUpDown.TabIndex = 4;
            this.axisDivisionNumericUpDown.ThousandsSeparator = true;
            this.axisDivisionNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // axisLenghtNumericUpDown
            // 
            this.axisLenghtNumericUpDown.DecimalPlaces = 1;
            this.axisLenghtNumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.axisLenghtNumericUpDown.Location = new System.Drawing.Point(46, 40);
            this.axisLenghtNumericUpDown.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.axisLenghtNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.axisLenghtNumericUpDown.Name = "axisLenghtNumericUpDown";
            this.axisLenghtNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.axisLenghtNumericUpDown.TabIndex = 3;
            this.axisLenghtNumericUpDown.ThousandsSeparator = true;
            this.axisLenghtNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(43, 99);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(128, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Длина разделителя (м):";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(43, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Цена деления (м):";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(43, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Длина (м):";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lightSaveButton);
            this.groupBox3.Controls.Add(this.ColorPanel);
            this.groupBox3.Controls.Add(this.changeColorButton);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.zLightNumericUpDown);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.yLightNumericUpDown);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.xLightNumericUpDown);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.diffuseTrack);
            this.groupBox3.Controls.Add(this.ambientTrack);
            this.groupBox3.Location = new System.Drawing.Point(12, 25);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(250, 323);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Освещение";
            // 
            // lightSaveButton
            // 
            this.lightSaveButton.Location = new System.Drawing.Point(80, 270);
            this.lightSaveButton.Name = "lightSaveButton";
            this.lightSaveButton.Size = new System.Drawing.Size(92, 35);
            this.lightSaveButton.TabIndex = 6;
            this.lightSaveButton.Text = "Сохранить";
            this.lightSaveButton.UseVisualStyleBackColor = true;
            this.lightSaveButton.Click += new System.EventHandler(this.lightSaveButton_Click);
            // 
            // ColorPanel
            // 
            this.ColorPanel.Location = new System.Drawing.Point(13, 216);
            this.ColorPanel.Name = "ColorPanel";
            this.ColorPanel.Size = new System.Drawing.Size(50, 44);
            this.ColorPanel.TabIndex = 13;
            // 
            // changeColorButton
            // 
            this.changeColorButton.Location = new System.Drawing.Point(69, 216);
            this.changeColorButton.Name = "changeColorButton";
            this.changeColorButton.Size = new System.Drawing.Size(136, 44);
            this.changeColorButton.TabIndex = 12;
            this.changeColorButton.Text = "Изменить цвет";
            this.changeColorButton.UseVisualStyleBackColor = true;
            this.changeColorButton.Click += new System.EventHandler(this.changeColorButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 193);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Цвет освещения:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(174, 164);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Z";
            // 
            // zLightNumericUpDown
            // 
            this.zLightNumericUpDown.DecimalPlaces = 1;
            this.zLightNumericUpDown.Location = new System.Drawing.Point(194, 162);
            this.zLightNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.zLightNumericUpDown.Minimum = new decimal(new int[] {
            9999,
            0,
            0,
            -2147483648});
            this.zLightNumericUpDown.Name = "zLightNumericUpDown";
            this.zLightNumericUpDown.Size = new System.Drawing.Size(50, 20);
            this.zLightNumericUpDown.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(84, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Y";
            // 
            // yLightNumericUpDown
            // 
            this.yLightNumericUpDown.DecimalPlaces = 1;
            this.yLightNumericUpDown.Location = new System.Drawing.Point(111, 162);
            this.yLightNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.yLightNumericUpDown.Minimum = new decimal(new int[] {
            9999,
            0,
            0,
            -2147483648});
            this.yLightNumericUpDown.Name = "yLightNumericUpDown";
            this.yLightNumericUpDown.Size = new System.Drawing.Size(50, 20);
            this.yLightNumericUpDown.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "X";
            // 
            // xLightNumericUpDown
            // 
            this.xLightNumericUpDown.DecimalPlaces = 1;
            this.xLightNumericUpDown.Location = new System.Drawing.Point(25, 162);
            this.xLightNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.xLightNumericUpDown.Minimum = new decimal(new int[] {
            9999,
            0,
            0,
            -2147483648});
            this.xLightNumericUpDown.Name = "xLightNumericUpDown";
            this.xLightNumericUpDown.Size = new System.Drawing.Size(50, 20);
            this.xLightNumericUpDown.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Вектор направления света:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Яркость рассеянного света";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Яркость фонового освещения";
            // 
            // diffuseTrack
            // 
            this.diffuseTrack.LargeChange = 2;
            this.diffuseTrack.Location = new System.Drawing.Point(6, 99);
            this.diffuseTrack.Name = "diffuseTrack";
            this.diffuseTrack.Size = new System.Drawing.Size(238, 45);
            this.diffuseTrack.TabIndex = 1;
            // 
            // ambientTrack
            // 
            this.ambientTrack.LargeChange = 2;
            this.ambientTrack.Location = new System.Drawing.Point(6, 35);
            this.ambientTrack.Name = "ambientTrack";
            this.ambientTrack.Size = new System.Drawing.Size(238, 45);
            this.ambientTrack.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox7);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Location = new System.Drawing.Point(503, 25);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(223, 460);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Камера";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.saveMouseSenseButton);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.cameraTranslateSpeedumericUpDown);
            this.groupBox7.Controls.Add(this.cameraRotateSpeedNumericUpDown);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.cameraZoomSpeedNumericUpDown);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Location = new System.Drawing.Point(6, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(202, 215);
            this.groupBox7.TabIndex = 22;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Чувствительность мыши";
            // 
            // saveMouseSenseButton
            // 
            this.saveMouseSenseButton.Location = new System.Drawing.Point(31, 179);
            this.saveMouseSenseButton.Name = "saveMouseSenseButton";
            this.saveMouseSenseButton.Size = new System.Drawing.Size(137, 23);
            this.saveMouseSenseButton.TabIndex = 20;
            this.saveMouseSenseButton.Text = "Сохранить";
            this.saveMouseSenseButton.UseVisualStyleBackColor = true;
            this.saveMouseSenseButton.Click += new System.EventHandler(this.saveMouseSenseButton_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(141, 26);
            this.label16.TabIndex = 17;
            this.label16.Text = "Скорость перетаскивания\r\n(правая клавиша мыши)";
            // 
            // cameraTranslateSpeedumericUpDown
            // 
            this.cameraTranslateSpeedumericUpDown.DecimalPlaces = 3;
            this.cameraTranslateSpeedumericUpDown.Location = new System.Drawing.Point(9, 50);
            this.cameraTranslateSpeedumericUpDown.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.cameraTranslateSpeedumericUpDown.Name = "cameraTranslateSpeedumericUpDown";
            this.cameraTranslateSpeedumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.cameraTranslateSpeedumericUpDown.TabIndex = 0;
            this.cameraTranslateSpeedumericUpDown.ThousandsSeparator = true;
            // 
            // cameraRotateSpeedNumericUpDown
            // 
            this.cameraRotateSpeedNumericUpDown.DecimalPlaces = 3;
            this.cameraRotateSpeedNumericUpDown.Location = new System.Drawing.Point(9, 96);
            this.cameraRotateSpeedNumericUpDown.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.cameraRotateSpeedNumericUpDown.Name = "cameraRotateSpeedNumericUpDown";
            this.cameraRotateSpeedNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.cameraRotateSpeedNumericUpDown.TabIndex = 1;
            this.cameraRotateSpeedNumericUpDown.ThousandsSeparator = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 124);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(184, 26);
            this.label18.TabIndex = 19;
            this.label18.Text = "Скорость увеличения/уменьшения\r\n(колесико мыши)";
            // 
            // cameraZoomSpeedNumericUpDown
            // 
            this.cameraZoomSpeedNumericUpDown.DecimalPlaces = 3;
            this.cameraZoomSpeedNumericUpDown.Location = new System.Drawing.Point(9, 153);
            this.cameraZoomSpeedNumericUpDown.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.cameraZoomSpeedNumericUpDown.Name = "cameraZoomSpeedNumericUpDown";
            this.cameraZoomSpeedNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.cameraZoomSpeedNumericUpDown.TabIndex = 2;
            this.cameraZoomSpeedNumericUpDown.ThousandsSeparator = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 80);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(152, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = "Скорость вращения камеры";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.SetCameraTargetButton);
            this.groupBox6.Controls.Add(this.cameraTargetXnumericUpDown);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.cameraTargetYnumericUpDown);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.cameraTargetZnumericUpDown);
            this.groupBox6.Location = new System.Drawing.Point(6, 346);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(202, 105);
            this.groupBox6.TabIndex = 21;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Смотреть на точку";
            // 
            // SetCameraTargetButton
            // 
            this.SetCameraTargetButton.Location = new System.Drawing.Point(138, 38);
            this.SetCameraTargetButton.Name = "SetCameraTargetButton";
            this.SetCameraTargetButton.Size = new System.Drawing.Size(40, 40);
            this.SetCameraTargetButton.TabIndex = 23;
            this.SetCameraTargetButton.Text = "ОК";
            this.SetCameraTargetButton.UseVisualStyleBackColor = true;
            this.SetCameraTargetButton.Click += new System.EventHandler(this.SetCameraTargetButton_Click);
            // 
            // cameraTargetXnumericUpDown
            // 
            this.cameraTargetXnumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.cameraTargetXnumericUpDown.Location = new System.Drawing.Point(32, 24);
            this.cameraTargetXnumericUpDown.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.cameraTargetXnumericUpDown.Minimum = new decimal(new int[] {
            276447231,
            23283,
            0,
            -2147483648});
            this.cameraTargetXnumericUpDown.Name = "cameraTargetXnumericUpDown";
            this.cameraTargetXnumericUpDown.Size = new System.Drawing.Size(78, 20);
            this.cameraTargetXnumericUpDown.TabIndex = 17;
            this.cameraTargetXnumericUpDown.ThousandsSeparator = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(14, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "X";
            // 
            // cameraTargetYnumericUpDown
            // 
            this.cameraTargetYnumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.cameraTargetYnumericUpDown.Location = new System.Drawing.Point(32, 50);
            this.cameraTargetYnumericUpDown.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.cameraTargetYnumericUpDown.Minimum = new decimal(new int[] {
            276447231,
            23283,
            0,
            -2147483648});
            this.cameraTargetYnumericUpDown.Name = "cameraTargetYnumericUpDown";
            this.cameraTargetYnumericUpDown.Size = new System.Drawing.Size(78, 20);
            this.cameraTargetYnumericUpDown.TabIndex = 19;
            this.cameraTargetYnumericUpDown.ThousandsSeparator = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 52);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "Y";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(12, 78);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(14, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "Z";
            // 
            // cameraTargetZnumericUpDown
            // 
            this.cameraTargetZnumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.cameraTargetZnumericUpDown.Location = new System.Drawing.Point(32, 76);
            this.cameraTargetZnumericUpDown.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.cameraTargetZnumericUpDown.Minimum = new decimal(new int[] {
            276447231,
            23283,
            0,
            -2147483648});
            this.cameraTargetZnumericUpDown.Name = "cameraTargetZnumericUpDown";
            this.cameraTargetZnumericUpDown.Size = new System.Drawing.Size(78, 20);
            this.cameraTargetZnumericUpDown.TabIndex = 21;
            this.cameraTargetZnumericUpDown.ThousandsSeparator = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.setCameraPositionButton);
            this.groupBox5.Controls.Add(this.cameraPositionXnumericUpDown);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.cameraPositionYnumericUpDown);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.cameraPositionZnumericUpDown);
            this.groupBox5.Location = new System.Drawing.Point(6, 240);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(202, 100);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Установить камеру в позицию:";
            // 
            // setCameraPositionButton
            // 
            this.setCameraPositionButton.Location = new System.Drawing.Point(138, 30);
            this.setCameraPositionButton.Name = "setCameraPositionButton";
            this.setCameraPositionButton.Size = new System.Drawing.Size(40, 40);
            this.setCameraPositionButton.TabIndex = 17;
            this.setCameraPositionButton.Text = "ОК";
            this.setCameraPositionButton.UseVisualStyleBackColor = true;
            this.setCameraPositionButton.Click += new System.EventHandler(this.setCameraPositionButton_Click);
            // 
            // cameraPositionXnumericUpDown
            // 
            this.cameraPositionXnumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.cameraPositionXnumericUpDown.Location = new System.Drawing.Point(32, 16);
            this.cameraPositionXnumericUpDown.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.cameraPositionXnumericUpDown.Minimum = new decimal(new int[] {
            276447231,
            23283,
            0,
            -2147483648});
            this.cameraPositionXnumericUpDown.Name = "cameraPositionXnumericUpDown";
            this.cameraPositionXnumericUpDown.Size = new System.Drawing.Size(78, 20);
            this.cameraPositionXnumericUpDown.TabIndex = 11;
            this.cameraPositionXnumericUpDown.ThousandsSeparator = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "X";
            // 
            // cameraPositionYnumericUpDown
            // 
            this.cameraPositionYnumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.cameraPositionYnumericUpDown.Location = new System.Drawing.Point(32, 42);
            this.cameraPositionYnumericUpDown.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.cameraPositionYnumericUpDown.Minimum = new decimal(new int[] {
            276447231,
            23283,
            0,
            -2147483648});
            this.cameraPositionYnumericUpDown.Name = "cameraPositionYnumericUpDown";
            this.cameraPositionYnumericUpDown.Size = new System.Drawing.Size(78, 20);
            this.cameraPositionYnumericUpDown.TabIndex = 13;
            this.cameraPositionYnumericUpDown.ThousandsSeparator = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Y";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Z";
            // 
            // cameraPositionZnumericUpDown
            // 
            this.cameraPositionZnumericUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.cameraPositionZnumericUpDown.Location = new System.Drawing.Point(32, 68);
            this.cameraPositionZnumericUpDown.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.cameraPositionZnumericUpDown.Minimum = new decimal(new int[] {
            276447231,
            23283,
            0,
            -2147483648});
            this.cameraPositionZnumericUpDown.Name = "cameraPositionZnumericUpDown";
            this.cameraPositionZnumericUpDown.Size = new System.Drawing.Size(78, 20);
            this.cameraPositionZnumericUpDown.TabIndex = 15;
            this.cameraPositionZnumericUpDown.ThousandsSeparator = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.ViewSaveButton);
            this.groupBox8.Controls.Add(this.viewDistNumericUpDown);
            this.groupBox8.Controls.Add(this.label23);
            this.groupBox8.Controls.Add(this.FOVnumericUpDown);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Location = new System.Drawing.Point(12, 354);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(250, 168);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Видимость";
            // 
            // ViewSaveButton
            // 
            this.ViewSaveButton.Location = new System.Drawing.Point(74, 121);
            this.ViewSaveButton.Name = "ViewSaveButton";
            this.ViewSaveButton.Size = new System.Drawing.Size(87, 32);
            this.ViewSaveButton.TabIndex = 4;
            this.ViewSaveButton.Text = "Сохранить";
            this.ViewSaveButton.UseVisualStyleBackColor = true;
            this.ViewSaveButton.Click += new System.EventHandler(this.ViewSaveButton_Click);
            // 
            // viewDistNumericUpDown
            // 
            this.viewDistNumericUpDown.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.viewDistNumericUpDown.Location = new System.Drawing.Point(14, 95);
            this.viewDistNumericUpDown.Maximum = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.viewDistNumericUpDown.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.viewDistNumericUpDown.Name = "viewDistNumericUpDown";
            this.viewDistNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.viewDistNumericUpDown.TabIndex = 3;
            this.viewDistNumericUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(11, 74);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(143, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Дальность прорисовки (м)";
            // 
            // FOVnumericUpDown
            // 
            this.FOVnumericUpDown.Location = new System.Drawing.Point(13, 39);
            this.FOVnumericUpDown.Maximum = new decimal(new int[] {
            179,
            0,
            0,
            0});
            this.FOVnumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.FOVnumericUpDown.Name = "FOVnumericUpDown";
            this.FOVnumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.FOVnumericUpDown.TabIndex = 1;
            this.FOVnumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 23);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Поле зрения (град.)";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.setDarkButton);
            this.groupBox9.Controls.Add(this.SetDarkCheckBox);
            this.groupBox9.Location = new System.Drawing.Point(268, 455);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(229, 67);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Темные тона";
            // 
            // setDarkButton
            // 
            this.setDarkButton.Location = new System.Drawing.Point(166, 20);
            this.setDarkButton.Name = "setDarkButton";
            this.setDarkButton.Size = new System.Drawing.Size(55, 33);
            this.setDarkButton.TabIndex = 1;
            this.setDarkButton.Text = "ОК";
            this.setDarkButton.UseVisualStyleBackColor = true;
            this.setDarkButton.Click += new System.EventHandler(this.setDarkButton_Click);
            // 
            // SetDarkCheckBox
            // 
            this.SetDarkCheckBox.AutoSize = true;
            this.SetDarkCheckBox.Location = new System.Drawing.Point(6, 29);
            this.SetDarkCheckBox.Name = "SetDarkCheckBox";
            this.SetDarkCheckBox.Size = new System.Drawing.Size(154, 17);
            this.SetDarkCheckBox.TabIndex = 0;
            this.SetDarkCheckBox.Text = "Установить темные тона";
            this.SetDarkCheckBox.UseVisualStyleBackColor = true;
            // 
            // FormVisualSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 534);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormVisualSettings";
            this.Text = "Настройки визуализации";
            this.Load += new System.EventHandler(this.FormVisualSettings_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tailNextPointAfterNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tailLengthNumericUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axisFontSizeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axisDivisionLenghtNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axisDivisionNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axisLenghtNumericUpDown)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zLightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yLightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xLightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diffuseTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ambientTrack)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraTranslateSpeedumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraRotateSpeedNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraZoomSpeedNumericUpDown)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraTargetXnumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraTargetYnumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraTargetZnumericUpDown)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraPositionXnumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraPositionYnumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraPositionZnumericUpDown)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewDistNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FOVnumericUpDown)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown tailNextPointAfterNumericUpDown;
        private System.Windows.Forms.NumericUpDown tailLengthNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar diffuseTrack;
        private System.Windows.Forms.TrackBar ambientTrack;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button changeColorButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown zLightNumericUpDown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown yLightNumericUpDown;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown xLightNumericUpDown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button saveTailsButton;
        private System.Windows.Forms.Button saveAxisesButton;
        private System.Windows.Forms.NumericUpDown axisDivisionLenghtNumericUpDown;
        private System.Windows.Forms.NumericUpDown axisDivisionNumericUpDown;
        private System.Windows.Forms.NumericUpDown axisLenghtNumericUpDown;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel ColorPanel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown cameraPositionZnumericUpDown;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown cameraPositionYnumericUpDown;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown cameraPositionXnumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraZoomSpeedNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraRotateSpeedNumericUpDown;
        private System.Windows.Forms.NumericUpDown cameraTranslateSpeedumericUpDown;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button SetCameraTargetButton;
        private System.Windows.Forms.NumericUpDown cameraTargetXnumericUpDown;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown cameraTargetYnumericUpDown;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown cameraTargetZnumericUpDown;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button setCameraPositionButton;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button saveMouseSenseButton;
        private System.Windows.Forms.Button lightSaveButton;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button ViewSaveButton;
        private System.Windows.Forms.NumericUpDown viewDistNumericUpDown;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.NumericUpDown axisFontSizeNumericUpDown;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown FOVnumericUpDown;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button setDarkButton;
        private System.Windows.Forms.CheckBox SetDarkCheckBox;
    }
}